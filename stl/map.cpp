#include <map>
using namespace std;

int main() {
   map<string, int> M; 
    M["Top"] = 1; 
    M["Coder"] = 2; 
    M["SRM"] = 10; 
   
   
    int x = M["Top"] + M["Coder"]; 
    
    if(M.find("SRM") != M.end()) { 
          M.erase(M.find("SRM")); // or even M.erase("SRM") 
     }
     
     tr(M, it) {
        cout << it->first << " " << it->second << endl;
     }
     
     
}