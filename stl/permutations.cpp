#include <vector>
#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

#define all(X) X.begin(), X.end()

int main() {
   string x = "hello";
   sort(all(x));
   do {
      cout << x <<endl;
   }
   while(next_permutation(all(x)));
}