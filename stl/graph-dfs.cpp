#include <vector>
#include <iostream>
using namespace std;

typedef vector<int> vi;
typedef vector<vi>  vii;

#define all(X) X.begin(), X.end()

void insert(vii& g, int x, int y) {
  g[x].push_back(y);
}

void dfs(int x) {

}

void do_dfs(vii& g, int nvertices) {

  vi visited(nvertices, 0);

  // If there is an unvisited node, call dfs on that node
  for ( int i = 0; i < g.size(); ++i )
    for_each(all(g[i]), dfs);
}

int main() {
  int nvertices, nedges;
  cin >> nvertices >> nedges;

  vi line(nvertices, 0);
  vii graph(10, line);


  for ( int i = 0; i < nedges; ++i ) {
    int t1, t2;
    cin >> t1 >> t2;
    insert(graph, t1, t2);
    insert(graph, t2, t1);
  }
  return 0;
}

