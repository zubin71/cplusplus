#include <vector>
#include <iostream>
using namespace std;

#define SZ(x) x.size()

void passing_a_vector(vector<int> &arg) {
   
}

int main() {
   typedef vector<int> vi;
   typedef vector<vi> vii;
   
   vi test;
   test.push_back(10);
   cout << test.empty() << endl;
   cout << SZ(test) << endl;
   
   test.resize(100);
   test.resize(10);
   test.resize(0);

   cout << test.empty() << endl;
   cout << SZ(test) << endl;

   // creating another vector from test
   vi another_vector(test);
   
   vi blah_one(10, -1);
   vii blah_two(2, blah_one);
   
   passing_a_vector(blah_one);

   
}