#include <iostream>
#include <sstream>
using namespace std;

void f(const string& s) { 

     // Construct an object to parse strings 
     istringstream is(s); 

     // Vector to store data 
     vector<int> v; 

     // Read integer while possible and add it to the vector 
     int tmp; 
     while(is >> tmp) { 
          v.push_back(tmp); 
     } 
}


string f(const vector<int>& v) { 

     // Constucvt an object to do formatted output 
     ostringstream os; 

     // Copy all elements from vector<int> to string stream as text 
     tr(v, it) { 
          os << ' ' << *it; 
     } 

     // Get string from string stream 
     string s = os.str(); 

     // Remove first space character 
     if(!s.empty()) { // Beware of empty string here 
          s = s.substr(1); 
     } 

     return s; 
}


int main() {
   
}