#include <set>
#include <iostream>
using namespace std;

#define tr(container, it) \ 
      for(typeof(container.begin()) it = container.begin(); it != container.end(); it++)



int main() {
   set<int> s;
   
   for(int i=0;i<100; ++i)
      s.insert(i);
   
   s.insert(42); // does nothing as it already exists
   
   s.erase(10); // remove 10 from the set
   
   cout << s.size() << endl;
   
   for ( set<int>::const_iterator it = s.begin(); it != s.end(); ++it )
      cout << *it << " ";
   cout << endl;
   
   set< pair<string, pair< int, vector<int> > > SS; 
   int total = 0; 
   tr(SS, it) { 
        total += it->second.first; 
   }
   
   // for set and map DO NOT use the global find, use the set::find
   
   if ( s.find(42) != s.end() ) {
      cout << "42 is present" << endl;
   }
   
   #define present(container, element) (container.find(element) != container.end()) 
   #define cpresent(container, element) (find(all(container),element) != container.end())
    
   set<int>::iterator it1, it2; 
   it1 = s.find(10); 
    it2 = s.find(100); 
    // Will work if it1 and it2 are valid iterators, i.e. values 10 and 100 present in set. 
    s.erase(it1, it2); // Note that 10 will be deleted, but 100 will remain in the container 
   
   
    int data[5] = { 5, 1, 4, 2, 3 }; 
    set<int> S(data, data+5);
    
    vector<int> v; 
    set<int> s(all(v)); 
    vector<int> v2(all(s)); // will have contents with duplicates removed and in ascending order
    
     
   
}