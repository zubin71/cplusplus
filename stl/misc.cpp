#include <map>
#include <vector>
#include <iostream>
using namespace std;

#define all(X) X.begin(), X.end()

int main() {
  map<string, string> mymap;
  mymap["asdf"] = "asdf";

  vector< pair<string, string> > myvector(all(mymap));

  // copy data between containers
  vector<int> v1;
  vector<int> v2;
  v1.resize(v1.size() + v2.size());
  copy(all(v2), v1.end()-v2.size());

  // more information at http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=standardTemplateLibrary2#algorithms
}
