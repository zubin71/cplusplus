#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

#define pprint(container, it) \
for (typeof(container.begin()) it = container.begin(); it!=container.end(); ++it) cout << *it << " ";
int main() {
   vector<int> test;
   test.push_back(1);
   test.push_back(2);
   reverse(test.begin(), test.end());
   pprint(test, it); cout<<endl;
   
   vector<int>::iterator it;
   vector<int>::const_iterator cit;
   vector<int>::reverse_iterator rit;
   //vector<int>::const_reverse_iterator it;
   
   // for iterators dont use <. use != instead
   vector<int> v;
   for ( int i = 1; i < 100; i++ ) {
      v.push_back(i*i);
   }
   
   int i = find ( v.begin(), v.end(), 49 ) - v.begin();
   if ( i < v.size() ) {
      cout <<" found at index - "<< i;
   }
/*   
   int data[5] = { 1, 5, 2, 4, 3 }; 
   vector<int> X(data, data+5); 
   int v1 = *max_element(X.begin(), X.end()); // Returns value of max element in vector 
   int i1 = min_element(X.begin(), X.end()) – X.begin; // Returns index of min element in vector 

   int v2 = *max_element(data, data+5); // Returns value of max element in array 
   int i3 = min_element(data, data+5) – data; // Returns index of min element in array
*/
   #define all(c) c.begin(), c.end()
   
   sort(all(test));
   test.insert(1, 234); // insert at index 1
   
   typedef vector<int> vi;
   
   vi one; vi two;
   one.push_back(1); one.push_back(2);
   two.push_back(3); two.push_back(4); two.push_back(5);
   one.insert(1, all(two));
   
   erase(all(two));
   
   
}