#include <iostream>
#include <sstream>
#include <cmath>
using namespace std;

// using STL to get real and decimal part
// using STL to split a string based on "."
/* how would math operations like the following work :-
   float x = 10.14;
   x &= 0xFFFF:
*/
// http://stackoverflow.com/questions/14303469/c-code-to-convert-a-fraction-to-binary
// how would this fare for negative nos?

double get_fractional_part(double num) {
   return num - floor(num);
}

double get_integral_part(double num) {
   return floor(num);
}

string fraction_to_binary(double num) {
   string decimal_binary = "";
   double decimal_part = get_fractional_part(num);
   while ( decimal_part > 0 ) {
      double temp = decimal_part * 2;
      if ( get_integral_part(temp) == 0 ) decimal_binary += "0";
      else                            decimal_binary += "1";
      decimal_part = get_fractional_part(temp); 
   }
   return decimal_binary;
}

string fraction_to_binary_2(double num) {
   string decimal_binary = "";
   double integral_part = 0;
   double decimal_part = modf(num, &integral_part);
   while ( decimal_part > 0 ) {
      double temp = decimal_part * 2;
      decimal_part = modf(temp, &integral_part);
      if ( integral_part == 0 )       decimal_binary += "0";
      else                            decimal_binary += "1"; 
   }
   return decimal_binary;
}

/********************************************************************************
method-2
*/

/*
   Every float can be expressed as "significant * (2^exponent)". 
   Significand is a float between 0.5 and 1.0
   Exponent is an integral value
*/
void method_2() {
   double num = 15.2;
   int significand = 0;
   double exponent = 0;
   
   exponent = frexp(num, &significand);
   cout << exponent << endl << significand << endl;
   
   double generated_num = ldexp(significand, exponent);
   
   cout << generated_num << endl;
   
}


int main() {/*
   cout << "3.50 - " << fraction_to_binary(3.50) << endl;
   cout << "3.14 - " << fraction_to_binary(3.14) << endl;
   cout << "3.50 - " << fraction_to_binary_2(3.50) << endl;
   cout << "3.14 - " << fraction_to_binary_2(3.14) << endl;

   float myFloat = 3.50;
   stringstream ss;
   ss << myFloat;
   cout << string(ss.str()) << endl;
   cout << get_integral_part(3.5) << endl; */
   method_2();
}