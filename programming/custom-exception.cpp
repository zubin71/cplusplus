#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

class CustomException : public std::runtime_error {
public:
  CustomException(std::string const &s) : std::runtime_error(s)
  {
     
  } 
};

int main() {
   throw CustomException("Yikes!");
}