#include <iostream>
using namespace std;

#define MAXV 1000

struct EDGENODE {
   int y;
   int weight;
   EDGENODE *next; 
};

struct GRAPH {
   EDGENODE *edges[MAXV+1];
   int degree[MAXV+1];
   int nvertices;
   int nedges;
   bool directed;
};

void initialize_graph(GRAPH *g, bool directed) {
   g->nvertices = 0;
   g->nedges = 0;
   g->directed = directed;
   for ( int i = 1; i <= MAXV; ++i ) {
      g->edges[i] = NULL;
      g->degree[i] = 0;
   }
}

void insert_edge(GRAPH *g, int x, int y, bool directed) {
   EDGENODE *temp = new EDGENODE();
   temp->y = y;
   temp->weight = -1;
   temp->next = g->edges[x];
   
   g->edges[x] = temp;

   if ( directed )
      insert_edge(g, y, x, false);
   else
      g->nedges ++;
}

int main() {
   GRAPH g;
   initialize_graph(&g, false);
   insert_edge(&g, 1, 2, false);
   insert_edge(&g, 2, 3, false);
   insert_edge(&g, 3, 4, false);
   insert_edge(&g, 4, 5, false);
}