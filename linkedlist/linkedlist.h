#ifndef  _HAVE_LINKEDLIST_H
#define  _HAVE_LINKEDLIST_H

#include <iostream>
using namespace std;

#include "../types.h"
#include "../debug.h"

struct LIST {
  s32 x;
  LIST *ptr;
};


void add_value(LIST **head, s32 x);
void traverse_list(LIST *head);
s32 delete_value(LIST **head, s32 value);
s32 remove_third_from_last(LIST *head);
s32 remove_duplicates(LIST **head);
s32 delete_list(LIST **head);

/* functions to implement :-
 * - sort the linked list
 * - implement a O(n^2) duplicate searching method
 * - create a BST from this and remove duplicates
 * - create a min heap from this and remove duplicates
 */

#endif                                                  /* ! _HAVE_LINKEDLIST_H */
