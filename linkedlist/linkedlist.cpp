#include "linkedlist.h"
#include "../debug.h"

#define DEBUG_BUILD 1

void add_value(LIST **head, s32 x) {
#define DEBUG_BUILD
  DEBUG ( "Adding value : " << x << endl );
  if ( *head == NULL ) {
    *head = new LIST();
    (*head)->x = x;
    (*head)->ptr = NULL;
  }
  else {
    LIST *temp = *head;
    while ( temp ) {
      if ( ! temp->ptr)
        break;
      temp = temp->ptr;
    }
    LIST *n = new LIST();
    n->x = x;
    n->ptr = NULL;
    temp->ptr = n;
  }
}

void traverse_list(LIST *head) {
  cout << "Traversing = " << endl;
  LIST *temp = head;

  while ( temp ) {
    cout << temp->x << endl;
    temp = temp->ptr;
  }
}

s32 delete_value(LIST **head, s32 value) {
  cout << "Removing value = " << value << endl;
  // If there are no elements
  if ( ! *head )
    return -1;

  // If there is only one element
  else if ( ! (*head)->ptr ) {
    if ( (*head)->x == value ) {
      delete(*head);
      *head = NULL;
      return 0;
    }
    return -1;
  }

  // If there are > 1 nodes, traverse and check for the node
  LIST *traverse = *head;

  // If the first node is the one with value
  if ( traverse->x == value ) {
    *head = (*head)->ptr;
    delete(traverse);
    return 0;
  }

  while ( traverse->ptr != NULL && (traverse->ptr)->x != value )
    traverse = traverse->ptr;

  if ( traverse->ptr && (traverse->ptr)->x == value ) {
    LIST *temp = traverse->ptr;

    // Is this the last node?
    if ( temp->ptr == NULL ) {
      traverse->ptr = NULL;
      delete(temp);
    }

    else {
      traverse->ptr = temp->ptr;
      delete(temp);
    }
  }
  return 0;
}



s32 remove_third_from_last(LIST *head) {
  cout << "Attempting to remove third from last" << endl;
  if ( head == NULL )
    return -1;

  LIST *traverse = head;
  LIST *prev = NULL;

  while ( traverse && traverse->ptr && (traverse->ptr)->ptr && ((traverse->ptr)->ptr)->ptr) {
    prev = traverse;
    traverse = traverse->ptr;
  }

  if ( traverse == head )
    return -1;

  else {
    prev->ptr = traverse->ptr;
    delete(traverse);
    return 0;
  }
  return -1;
}

s32 remove_duplicates(LIST **head) {
  if ( *head == NULL ) return -1;
  else if ( (*head)->ptr == NULL ) return 0;

  LIST *first = *head;
  LIST *second = *head;

  while ( first ) {
    while ( second->ptr ) {
      if ( first->x == (second->ptr)->x) {
        LIST *temp = second->ptr;
        second->ptr = temp->ptr;
        delete(temp);
      }
      second = second->ptr;
    }
    first = first->ptr;
    second = first;
  }
  return 0;
}

s32 delete_list(LIST **head) {
  if ( *head == NULL ) return -1;

  LIST *traverse = *head;

  while ( traverse ) {
    LIST *temp = traverse;
    traverse = traverse->ptr;
    delete(temp);
  }
  *head = NULL;
  return 0;
}

/* functions to implement :-
 * - sort the linked list
 * - implement a O(n^2) duplicate searching method
 * - create a BST from this and remove duplicates
 * - create a min heap from this and remove duplicates
 */

