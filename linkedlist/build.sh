g++ -dynamiclib -o liblinkedlist.dylib linkedlist.cpp
g++ -L. -llinkedlist main.cpp -o main
./main
valgrind --tool=memcheck --leak-check=yes --leak-check=full --show-reachable=yes ./main
