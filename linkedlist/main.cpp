#include <iostream>
using namespace std;

#include "linkedlist.h"

s32 main() {
  LIST *head = NULL;
  s32 val = 5;
  add_value(&head, val);
  add_value(&head, 10);
  add_value(&head, 20);
  traverse_list(head);
  delete_value(&head, 10);
  traverse_list(head);
  delete_value(&head, 5);
  traverse_list(head);
  delete_value(&head, 20);
  traverse_list(head);
cout << remove_third_from_last(head) << endl;

  add_value(&head, 10);
  add_value(&head, 20);
  add_value(&head, 30);
  add_value(&head, 40);

traverse_list(head);
  cout << remove_third_from_last(head);
  traverse_list(head);

  add_value(&head, 10);
  add_value(&head, 20);
  add_value(&head, 30);
  traverse_list(head);

  delete_list(&head);
//  remove_duplicates(&head);
//  traverse_list(head);
}
