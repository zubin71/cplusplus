#ifndef  _HAVE_DEBUG_H
#define  _HAVE_DEBUG_H

#ifdef DEBUG_BUILD
#  define DEBUG(x) cerr << x
#else
#  define DEBUG(x) do {} while(0)
#endif                                            /* ^DEBUG_BUILD     */

#endif                                            /* ! _HAVE_DEBUG_H  */
