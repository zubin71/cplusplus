#include <iostream>
using namespace std;

struct TREE {
  int x;
  TREE *left;
  TREE *right;
};

void insert(TREE **root, int value) {
  if ( *root == NULL ) {
    *root = new TREE();
    (*root)->x = value;
    (*root)->left = (*root)->right = 0;
    return ;
  }

  if ( value < (*root)->x )
    insert( &((*root)->left), value );
  else
    insert( &((*root)->right), value );
}

void in_order_traverse(TREE *root) {
   if ( root->left != NULL )
      in_order_traverse(root->left);
   cout << root->x << " ";
   if ( root-> right != NULL )
      in_order_traverse(root->right);
}

void breadth_first_traverse(TREE *root) {
   if ( root == NULL )
      return;
   cout << root->x << " ";
   breadth_first_traverse( root->left );
   breadth_first_traverse( root->right );
}

void depth_first_traverse(TREE *root) {
   if ( root == NULL )
      return;
   depth_first_traverse( root->left );
   depth_first_traverse( root->right );
   cout << root->x << " ";
}

void delete_tree(TREE **root) {
   if ( *root == NULL ) return;
   
   delete_tree( &((*root)->left) );
   delete_tree( &((*root)->right) );
   
   delete(*root);
}

int min_height(TREE *root) {
   if ( root == NULL )
      return 0;
   return 1 + min(min_height(root->left), min_height(root->right));
}

int max_height(TREE *root) {
   if ( root == NULL )
      return 0;
   return 1 + max(max_height(root->left), max_height(root->right));
}

bool is_balanced(TREE *root) {
   if ( (max_height(root) - min_height(root)) > 1 )
      return false;
   return true;
}

int main() {
  TREE *root = NULL;
  insert(&root, 2);
  insert(&root, 1);
  insert(&root, 3);
  
  cout << "value at root - " << root->x <<endl;
  cout << "In order traversal - "; in_order_traverse(root); cout<<endl;
  cout << "Breadth first traversal - "; breadth_first_traverse(root); cout<<endl;
  cout << "Depth first traversal - "; depth_first_traverse(root); cout<<endl;
  cout << "Balanced - " << is_balanced(root) << endl;
  delete_tree(&root);
}


/*
I wrote a simple program in C++ and I tried running valgrind against 
it to understand how memory would be managed. I ran valgrid as follows :-

valgrind --tool=memcheck --leak-check=yes --leak-check=full --show-reachable=yes ./tree

Part of the output I got had the following :-

==1265== LEAK SUMMARY:
==1265==    definitely lost: 0 bytes in 0 blocks
==1265==    indirectly lost: 0 bytes in 0 blocks
==1265==      possibly lost: 0 bytes in 0 blocks
==1265==    still reachable: 4,396 bytes in 8 blocks
==1265==         suppressed: 60 bytes in 1 blocks

I'm curious about what the "still reachable"
*/